from django.urls import include, path
from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register(r'notes', views.NotesViewSet)
router.register(r'stations', views.StationsViewSet, basename='stations')
router.register(r'departures', views.DeparturesViewSet, basename='departures')

urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]