import os
import requests

ns_stations_url = 'https://gateway.apiportal.ns.nl/reisinformatie-api/api/v2/stations'
ns_departures_url = 'https://gateway.apiportal.ns.nl/reisinformatie-api/api/v2/departures'


def has_search_term_in_array(search_term, item, key):
    keys_in_upper = [key.upper() for key in item[key]]
    # these comprehensions, they are awesome, but I'm not used to it
    return [key for key in keys_in_upper if search_term.upper() in key]


def has_search_term_in_dict(search_term, item, key):
    values_in_upper = [value.upper() for value in item[key].values()]
    return [value for value in values_in_upper if search_term.upper() in value]


# I tried to follow this tutorial for the .env
# https://alicecampkin.medium.com/how-to-set-up-environment-variables-in-django-f3c4db78c55f
# but I couldn't use env(), instead I'm now using os.environ
def get_ns_request(url, params=None):
    headers = {'Ocp-Apim-Subscription-Key': os.environ.get('NS_VALUE')}
    try:
        result = requests.get(url, headers=headers, params=params)
    except requests.exceptions.HTTPError as e:
        return SystemExit(e)

    if result.ok:
        return result.json()['payload'], result.status_code
    else:
        return result.json(), result.status_code
