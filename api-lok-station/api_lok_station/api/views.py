from rest_framework import viewsets
from rest_framework.response import Response
from .serializers import NoteSerializer
from .models import Note
from .helpers import get_ns_request, ns_stations_url, ns_departures_url, has_search_term_in_array, \
    has_search_term_in_dict
import requests


# Create your views here.
class StationsViewSet(viewsets.ViewSet):
    def list(self, request):
        try:
            search = request.GET.get('search', '')
            result = get_ns_request(ns_stations_url)
            # search on code and station names should be ok and give back all matching results
            stations = list(filter(
                lambda station: station['land'] == "NL" and
                (
                    search.upper() in station['code'] or
                    has_search_term_in_array(search, station, 'synoniemen') or
                    has_search_term_in_dict(search, station, 'namen')
                ), result[0]
            ))
            # I would like a more graceful way for handling this
            if len(stations) == 0:
                return Response({"error": "No stations found"}, status=404)
            else:
                return Response(stations, result[1])
        except requests.exceptions.HTTPError as e:
            return SystemExit(e)


class DeparturesViewSet(viewsets.ViewSet):
    def list(self, request):
        try:
            station_code = request.GET.get('station', '')
            result = get_ns_request(ns_departures_url, params={'station': station_code})
            return Response(result[0]['departures'], status=result[1])
        except requests.exceptions.HTTPError as e:
            return SystemExit(e)


# not using it atm
class NotesViewSet(viewsets.ModelViewSet):
    queryset = Note.objects.all().order_by('title')
    serializer_class = NoteSerializer
