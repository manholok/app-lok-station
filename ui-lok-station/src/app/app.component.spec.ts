import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  let spectator: Spectator<AppComponent>;
  const createComponent = createComponentFactory(AppComponent);

  beforeEach(() => {
    spectator = createComponent();
  });

  it('should create the app', () => {
    expect(spectator.component).toBeTruthy();
  });

  it(`should have as title 'ui-lok-station'`, () => {
    expect(spectator.component.title).toEqual('UI-lok-station');
  });

  it('should render title', () => {
    expect(spectator.query('h1')).toHaveText('UI-lok-station');
  });
});
