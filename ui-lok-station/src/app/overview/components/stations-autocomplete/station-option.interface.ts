interface StationOption {
  code: string;
  synoniemen: string[];
  namen: {
    lang: string;
    middel: string;
    kort: string;
  };
}

export {
  StationOption
}
