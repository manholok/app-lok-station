import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import {
  catchError,
  debounceTime,
  distinctUntilChanged,
  EMPTY,
  map,
  Observable,
  repeat,
  switchMap,
  tap
} from 'rxjs';
import { StationsService } from '@app/shared/services/stations/stations.service';
import { StationOption } from '@app/overview/components/stations-autocomplete/station-option.interface';

// It feels kind of weird to not use this autocomplete in a formGroup
@Component({
  selector: 'lok-stations-autocomplete',
  templateUrl: './stations-autocomplete.component.html',
  styleUrls: [ './stations-autocomplete.component.scss' ]
})
export class StationsAutocompleteComponent implements OnInit {
  @Output()
  public stationSelected = new EventEmitter<string>();

  public stationsControl = new FormControl();
  public filteredOptions: Observable<StationOption[]> | undefined;

  constructor(private stationsService: StationsService) {
  }

  ngOnInit(): void {
    this.filteredOptions = this.stationsControl.valueChanges.pipe(
      distinctUntilChanged(),
      debounceTime(400),
      tap(() => this.stationsControl.setErrors(null)),
      switchMap(searchTerm => {
          return this.stationsService.getStations(searchTerm);
        }
      ),
      catchError(err => {
        this.stationsControl.markAsTouched();
        this.stationsControl.setErrors({ noStations: true });
        return EMPTY;
      }),
      repeat(),
      map(response => {
        if (this.stationsControl.value) {
          this.stationSelected.emit(this.stationsControl.value);
        }
        return response;
      })
    );
  }

  getNiceTitle(option: StationOption): string {
    return option?.namen.lang || option?.code || '';
  }
}

