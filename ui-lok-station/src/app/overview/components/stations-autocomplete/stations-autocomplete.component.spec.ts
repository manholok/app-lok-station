import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { HttpClient } from '@angular/common/http';
import { StationsAutocompleteComponent } from './stations-autocomplete.component';
import { MatAutocomplete } from '@angular/material/autocomplete';

describe('StationsAutocompleteComponent', () => {
  let spectator: Spectator<StationsAutocompleteComponent>
  const createComponent = createComponentFactory({
    declarations: [ MatAutocomplete ],
    component: StationsAutocompleteComponent,
    mocks: [ HttpClient ]
  });

  beforeEach(async () => {
    spectator = createComponent();
  });

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
