import { Component, Input, OnInit } from '@angular/core';
import { DepartureTypes, TrainCategoryLabels } from '@app/shared/services/departures/departure.types';

@Component({
  selector: 'lok-direction-card',
  templateUrl: './direction-card.component.html',
  styleUrls: ['./direction-card.component.scss']
})
export class DirectionCardComponent implements OnInit {
  @Input()
  public data: DepartureTypes | undefined;

  constructor() { }

  ngOnInit(): void {
  }

  getLabel(category: string) {
    return TrainCategoryLabels[category as keyof typeof TrainCategoryLabels];
  }
}
