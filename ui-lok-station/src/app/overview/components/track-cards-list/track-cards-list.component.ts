import { Component, Input, OnInit } from '@angular/core';
import { DepartureTypes } from '@app/shared/services/departures/departure.types';

@Component({
  selector: 'lok-track-cards-list',
  templateUrl: './track-cards-list.component.html',
  styleUrls: ['./track-cards-list.component.scss']
})
export class TrackCardsListComponent implements OnInit {

  @Input()
  public departures: DepartureTypes[] | null = [];

  constructor() { }

  ngOnInit(): void {}

  trackByIndex(index: number) {
    return index;
  }

  getDeparturesByPlatform() {
    // get array of departures
    if (!this.departures) return [];

    // group by train platform/plannedTrack
    const list = this.departures.reduce((prev: DepartureTypes[][], curr: DepartureTypes) => {
      if (parseInt(curr.plannedTrack)) {
        prev[parseInt(curr.plannedTrack)] = [
          ...prev[parseInt(curr.plannedTrack)] ?? [],
          curr
        ]
      }
      return prev;
    }, [])

    return list;
  }

  getFirstDeparture() {
    if (!this.departures) return null;
    return this.departures[0];
  }
}


