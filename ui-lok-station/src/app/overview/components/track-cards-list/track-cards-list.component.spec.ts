import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackCardsListComponent } from './track-cards-list.component';

describe('TrackCardsListComponent', () => {
  let component: TrackCardsListComponent;
  let fixture: ComponentFixture<TrackCardsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrackCardsListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackCardsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
