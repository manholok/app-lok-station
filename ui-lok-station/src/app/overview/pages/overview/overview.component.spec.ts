import { OverviewComponent } from './overview.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { HttpClient } from '@angular/common/http';

describe('OverviewComponent', () => {
  let spectator: Spectator<OverviewComponent>;
  const createComponent = createComponentFactory({
    component: OverviewComponent,
    mocks: [ HttpClient ]
  })

  beforeEach(async () => {
    spectator = createComponent();
  });

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
