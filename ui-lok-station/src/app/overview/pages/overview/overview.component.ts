import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { DeparturesService } from '@app/shared/services/departures/departures.service';
import { DepartureTypes } from '@app/shared/services/departures/departure.types';
import { StationOption } from '@app/overview/components/stations-autocomplete/station-option.interface';

@Component({
  selector: 'lok-overview',
  templateUrl: './overview.component.html',
  styleUrls: [ './overview.component.scss' ]
})
export class OverviewComponent implements OnInit {
  public departures$: Observable<DepartureTypes[]> = of([]);

  constructor(private departuresService: DeparturesService) {
  }

  ngOnInit(): void {
  }

  onStationSelected(event: string | StationOption) {
    const stationCode = typeof event !== 'string' ? event.code : event;
    this.departures$ = this.departuresService.getDepartures(stationCode);
  }
}
