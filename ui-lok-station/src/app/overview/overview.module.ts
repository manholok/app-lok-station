import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgPipesModule } from 'ngx-pipes';
import { OverviewComponent } from './pages/overview/overview.component';
import { StationsAutocompleteComponent } from './components/stations-autocomplete/stations-autocomplete.component';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { DirectionCardComponent } from './components/direction-card/direction-card.component';
import { TrackCardsListComponent } from './components/track-cards-list/track-cards-list.component';

@NgModule({
  declarations: [
    OverviewComponent,
    StationsAutocompleteComponent,
    DirectionCardComponent,
    TrackCardsListComponent
  ],
  imports: [
    CommonModule,
    NgPipesModule,
    ReactiveFormsModule,
    SharedModule
  ],
})
export class OverviewModule {
}
