import { Component } from '@angular/core';

@Component({
  selector: 'lok-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'UI-lok-station';
}
