import { NgModule } from '@angular/core';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

// Could make a separate module for this, but it's not necessary for now
const materialModules = [
  MatAutocompleteModule,
  MatFormFieldModule,
  MatInputModule,
]

@NgModule({
  imports: [
    materialModules
  ],
  exports: [
    materialModules
  ]
})
export class SharedModule { }
