import { createHttpFactory, HttpMethod, SpectatorHttp } from '@ngneat/spectator';
import { StationsService } from './stations.service';

describe('StationsService', () => {

  let spectator: SpectatorHttp<StationsService>;
  const createHttp = createHttpFactory(StationsService);

  beforeEach(() => {
    spectator = createHttp();
  });

  it('should be created', () => {
    expect(spectator.service).toBeTruthy();
  });

  it('should call httpclient with gvc param', () => {
    spectator.service.getStations('gvc').subscribe();
    spectator.expectOne('/api/stations/?search=gvc', HttpMethod.GET);
  });
});
