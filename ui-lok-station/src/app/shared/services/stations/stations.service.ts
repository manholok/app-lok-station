import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, EMPTY, Observable, of, throwError } from 'rxjs';
import { StationOption } from '@app/overview/components/stations-autocomplete/station-option.interface';

@Injectable({
  providedIn: 'root'
})
export class StationsService {

  constructor(private http: HttpClient) { }

  getStations(search: string | StationOption): Observable<StationOption[]> {
    if (!search) return of([]);

    const code = typeof search === 'string' ? search : search?.code ?? '';

    return this.http.get<StationOption[]>('/api/stations/', {
      params: {
        search: code
      }
    }).pipe(
      catchError(err => throwError(err))
    );
  }
}
