import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { DepartureTypes } from '@app/shared/services/departures/departure.types';

@Injectable({
  providedIn: 'root'
})
export class DeparturesService {

  constructor(private http: HttpClient) { }

  getDepartures(station: string): Observable<DepartureTypes[]> {
    if (!station) return of([]);

    return this.http.get<DepartureTypes[]>('/api/departures/', {
      params: {
        station
      }
    });
  }
}
