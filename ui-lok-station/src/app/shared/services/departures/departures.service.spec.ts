import { DeparturesService } from './departures.service';
import { createHttpFactory, SpectatorService } from '@ngneat/spectator';

describe('DeparturesService', () => {
  let spectator: SpectatorService<DeparturesService>;
  const createHttp = createHttpFactory(DeparturesService);

  beforeEach(() => {
    spectator = createHttp();
  });

  it('should be created', () => {
    expect(spectator.service).toBeTruthy();
  });
});
