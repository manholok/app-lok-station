interface DepartureTypes {
  direction: string;
  plannedDateTime: string; // Datetime
  plannedTrack: string;
  trainCategory: TrainCategoryType;
}

enum TrainCategoryType {
  SPR = 'SPR',
  IC = 'IC'
}

const TrainCategoryLabels = {
  SPR: 'Sprinter',
  IC: 'Intercity'
}

export {
  DepartureTypes,
  TrainCategoryType,
  TrainCategoryLabels
}
