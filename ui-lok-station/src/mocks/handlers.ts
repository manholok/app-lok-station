import { rest } from 'msw';

const handlers: any[] = [
  rest.get('/api/stations', (req, res, ctx) => {
    const searchTerm = req.url.searchParams.get('search')?.toUpperCase() || '';
    const allStations = ['ASD', 'GVC', 'UT'];
    const filteredStations = allStations.filter(station => station.includes(searchTerm)) || [];

    return res(
      ctx.status(200),
      ctx.json([...filteredStations])
    );
  }),
];

export {
  handlers
}
