import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { worker } from './browser';

const bootstrap = () => {
  platformBrowserDynamic().bootstrapModule(AppModule)
    .catch(err => console.error(err));
}

if (environment.production) {
  enableProdMode();
}

if (environment.mswjs) {
  // Need to wait for the worker to be ready before starting the app
  worker.start().then(() => {
    bootstrap();
  });
} else {
  bootstrap();
}
