# Run Api without pipenv shell
go inside app-lok-station/api-lok-station
The folder with the Pipfile, then run the manage.py that inside the second api_lok_station

```
pipenv run python ./api_lok_station/manage.py runserver
```
### Resources used for api python
- https://pipenv.pypa.io/en/latest/
- https://medium.com/swlh/building-rest-api-with-django-rest-framework-77637dd51fe5


# Run Frontend with proxy
```
npm run start:proxy
```

# Run Frontend with MOCK (not completed)
```
npm run start
```
go into environment.ts and set mswjs to true
```
mswjs: true
```

# Questions
## UI-LOK-STATION
- I could not catch my backend errors without crashing the app, how can I make this work?

## API-LOK-STATION
- settings.py feels like a package.json, but there is also a PipFile...? How does this work with eachother? Is settings.py only something for django?
- is Django like Angular? so settings.py is like angular.json

# Notes
## UI-LOK-STATION
- Spectator for unit tests - https://github.com/ngneat/spectator
- I installed ngx-pipes, I don't think I use it anymore, first I tried to use a groupBy pipe, in the end I use a reducer function
- (not done)ngneat forms, or formly or just reactive forms angular
  - https://github.com/ngneat/forms-manager
  - Because angular is so verbose, these libs help it make less verbose
  
- ### CSS: I like a good utility library
  - https://tailwindcss.com with @apply in the scss
  - I use @apply to keep the html uncluttered
- ### 2 ways for api responses
  - use mswjs  npx msw init src --save, npm run start:mswjs
    - mocks, https://mswjs.io/docs/comparison
    - mockServiceWorker has been added to the assets of angular.json build, I would like it separated...
  - or use npm run start:proxy
- TODO: For the notifications I use some colored toasters
## API-LOK-STATION
- ofcourse, normally you are not supposed to upload .env, but for this assesment, I guess it is needed for demo purpose'
- I had a bit trouble with capitalization. [example: urlpatterns VS urlPatterns]. I'm used to camelCase

  - For baseUrls, secrets, header configs, for everything that you don't want to commit into git
- I am not familiar enough to create a dockerFile of this python application
- Maybe I could do some more data sorting in the backend:
  - fetching data
  - extract the data and save in db in a way that is easier for the frontend
  - Because at this moment I can't reach the NS api docs to see how I can alter the response and right now it is just too much. The frontend does not need much right now.
- proxy-ing to /api/stations/ (watch the slash at the end) works, but not without a slash. So don't forget the slash...
- about versioning, I guess I should use this, but I am not sure how this works: 
  - https://www.django-rest-framework.org/api-guide/versioning/
  - I also don't think that this is my main focus for the assessment as frontend-developer that will sometimes go into the backend.
  - But I am still interested